function openForm() {
    document.getElementById("myForm").style.display = "block";
    window.history.pushState('', '', '#form');
  }
  
  function closeForm() {
    window.history.back();
    document.getElementById("myForm").style.display = "none";
  } 
  window.addEventListener('popstate', function(e) {
    closeForm();
    window.history.back();
});